public class ArgumentMain {
    public static void main(String[] args) {
        int hasilPenjumlahan = 0 ;

        for(String arg: args){
            try {
                int inputNumerik = Integer.parseInt(arg);
                System.out.println(inputNumerik);
                hasilPenjumlahan += inputNumerik;

            } catch (Exception e) {
                System.out.println(arg + " bukan termasuk number");
            }
        }

        System.out.println("Jadi Hasil Penjumlahan = " + hasilPenjumlahan);
    }
}
