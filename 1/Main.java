import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {
    public static void main(String[] args) throws IOException {
        createFolder();

        for (int i = 1; i <= 10; i++) {
            createFileInFolder(i);
        }
    }

    public static void createFolder() {
        String folderName = "form1";
        File folder = new File(folderName);

        if (!folder.exists()) {
            if (folder.mkdir()) {
                System.out.println("Folder sukses dibuat.");
            } else {
                System.out.println("Folder gagal dibuat");
            }
        } else {
            System.out.println("Folder sudah ada");
        }
    }

    public static void createFileInFolder(int i) throws IOException {
        StringBuilder fileNameBuilder = new StringBuilder("form1/file");
        fileNameBuilder.append(i);
        fileNameBuilder.append(".txt");

        String fileName = fileNameBuilder.toString();
        File file = new File(fileName);

        PrintWriter pw = new PrintWriter(file);
        pw.println("Hello Java " + i);
        pw.close();

        if (!file.exists()) {
            if (file.createNewFile()) {
                System.out.println("File sukses dibuat");
            } else {
                System.out.println("File gagal dibuat");
            }
        } else {
            System.out.println("File sudah ada");
        }
    }
}
